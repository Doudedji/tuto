from flask import Flask
from flask_bootstrap import Bootstrap
from flask_script import Manager
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
import os.path
# pour la connection avec Bootstrap

def mkpath(p):
    return os.path.normpath(os.path.join(os.path.dirname(__file__),p))

app = Flask(__name__)
app.debug = True
app.config['BOOTSTRAP_SERVE_LOCAL']=True
app.config['SQLALCHEMY_TRACK_MODIFICATIONS']=True
app.config['SECRET_KEY']='Thisissupposedtobesecret!'
app.config['SQLALCHEMY_DATABASE_URI'] = ('sqlite:///'+mkpath('../music.db'))

Bootstrap(app)
db = SQLAlchemy(app)
manager = Manager(app)

login_manager = LoginManager(app)
