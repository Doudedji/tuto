from flask import Flask, render_template, redirect, url_for
from wtforms import StringField, PasswordField, BooleanField, HiddenField
from wtforms.validators import InputRequired, Email, Length, DataRequired
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user
from flask_wtf import FlaskForm
from .app import app, db
from flask import render_template, request, url_for, redirect
from .models import Requetes, Artiste, Titre, Musique, Genre, Combinaison


# *******************************    *************************************
#                     Connection des utilisateurs
# *******************************    *************************************
login_manager = LoginManager(app)
login_manager.init_app(app)
login_manager.login_view = 'login'

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(15), unique=True)
    email = db.Column(db.String(50), unique=True)
    password = db.Column(db.String(80))

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)

class LoginForm(FlaskForm):
    username = StringField('username', validators = [InputRequired(), Length(min=4,max=15)])
    password = PasswordField('password', validators = [InputRequired(), Length(min=8,max=80)])
    remember = BooleanField('remember me')

class RegisterForm(FlaskForm):
    email = StringField('email', validators = [InputRequired(), Email(message='Invalid email'), Length(min=4,max=15)])
    username = StringField('username', validators = [InputRequired(), Length(min=4,max=15)])
    password = PasswordField('password', validators = [InputRequired(), Length(min=8,max=80)])

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm(request.form)

    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user:
            if check_password_hash(user.password, form.password.data):
                login_user(user, remember=form.remember.data)
                return redirect(url_for('dashboard'))

        return '<h1>Invalid username or password</h1>'
        # return '<h1>' + form.username.data + ' ' + form.password.data + '</h1>'

    return render_template('login.html', form=form)

@app.route('/signup', methods=['GET', 'POST'])
def signup():
    form = RegisterForm()

    if form.validate_on_submit():
        hashed_password = generate_password_hash(form.password.data, method='sha256')
        new_user = User(username=form.username.data, email=form.email.data, password=hashed_password)
        db.session.add(new_user)
        db.session.commit()
        #return redirect(url_for('dashboard'))
        return '<h1>New user has been create!</h1>'
        # return '<h1>' + form.username.data + ' ' + form.email.data + ' ' + form.password.data + '</h1>'

    return render_template('signup.html', form=form)

@app.route('/dashboard')
@login_required
def dashboard():
    return render_template('dashboard.html', name=current_user.username)

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))


# ************************************************* ********************************************
# ************************************************* ********************************************
										# Pour les autres
# ************************************************* ********************************************
# ************************************************* ********************************************

class ArtisteForm(FlaskForm):
	id = HiddenField('id')
	by = StringField('Nom de l artiste :', validators=[DataRequired()])

class TitreForm(FlaskForm):
	id = HiddenField('id')
	title = StringField('Nom du titre', validators=[DataRequired()])

class GenreForm(FlaskForm):
	id = HiddenField('id')
	genre = StringField('Nom du genre', validators=[DataRequired()])

class CombinaisonForm(FlaskForm):
	id = StringField('Identifiant :', validators=[DataRequired()])
	musique_id = StringField('Numero de la musique', validators=[DataRequired()])
	genre_id = StringField('Numero du genre', validators=[DataRequired()])

class MusiqueForm(FlaskForm):
	entryId = StringField('Identifiant :', validators=[DataRequired()])
	img = StringField('Lien vers image :', validators=[DataRequired()])
	parent = StringField('parent :', validators=[DataRequired()])
	releaseYear= StringField('Annee de sortie :', validators=[DataRequired()])
	titre_id = StringField('Numero du titre :', validators=[DataRequired()])
	artiste_id = StringField('Numero de artiste :', validators=[DataRequired()])


@app.route('/')
def homepage():
	return render_template('index.html', title="Projet Flask.com", musics=Requetes.get_sample())

##### Vue Artiste
@app.route('/consultation/artiste')
def page_artiste():
	return render_template('consultation.html', title="artiste", dashboard=Artiste.query.all())

@app.route('/artiste/<int:id>')
def one_artiste(id):
	return render_template('musique.html', numero=id, title=Requetes.get_artiste_id(id), musics=Requetes.get_musiques_artiste_id(id), lien='artiste', id=id)

@app.route('/artiste/edit/<int:id>')
def edit_Artiste(id):
	a = Requetes.get_artiste_id(id)
	f = ArtisteForm(id=a.id, by=a.by)
	return render_template(
	"edit-artiste.html",
	artiste=a, form=f, lien='save', lien2='artiste'
	)

@app.route('/artiste/create/')
def create_Artiste():
	f= ArtisteForm()
	return render_template(
	"edit-artiste.html", form=f, lien='create', lien2='artiste'
	)

@app.route('/artiste/delete/<int:id>')
def supprimer_artiste(id):
	nom = Requetes.get_artiste_id(id)
	db.session.delete(nom)
	db.session.commit()
	return redirect(url_for('page_artiste'))

@app.route("/save/artiste/",methods=("POST",))
def save_Artiste():
	a = None
	f = ArtisteForm()
	if f.validate_on_submit():
		# On recupere valeur de l'id dans le formulaire
		id = int(f.id.data)
		a = Requetes.get_artiste_id(id)
		a.by = f.by.data
		db.session.commit()
		return redirect(url_for('one_artiste', id=a.id))
	a = Requetes.get_artiste_id(int(f.id.data))
	return render_template("accueil.html", artiste=a, form=f)

# ajout d'un artiste
@app.route('/create/artiste/', methods=("POST",))
def ajouter_artiste():
	a = None
	f = ArtisteForm()
	# en cas de validation
	if f.validate_on_submit():
		# On recupere le nom contenu dans l'emplacement 'by' du formulaire
		name = str(f.by.data)
		# On recupere le nom (dans une requete avec id) permettant de comparer avec le resultat precedant obtenu
		req = Requetes.get_artiste_name(name)
		if len(req) == 0:
			# On ajoute le nom dans la table Artiste
			ajout= Artiste(by = name)
			db.session.add(ajout)
			db.session.commit()
			print('enregistre')
		else:
			print('pas enregistre')
	# On se redirige vers la page artiste
	return redirect(url_for('page_artiste'))

##### Vue Titre
@app.route('/titre/')
def page_titre():
	return render_template('accueil.html', title="titre", accueil=Requetes.get_titres_all())

@app.route('/titre/<int:id>')
def one_titre(id):
	return render_template('musique.html', numero=id ,title=Requetes.get_titre_id(id), musics=Requetes.get_musiques_titre_id(id), lien='titre', id=id)

@app.route('/titre/edit/<int:id>')
def edit_Titre(id):
	a = Requetes.get_titre_id(id)
	f = TitreForm(id=a.id, title=a.title)
	return render_template(
	"edit-titre.html",
	artiste=a, form=f, lien='save', lien2='titre'
	)

@app.route('/titre/create/')
def create_Titre():
	f= TitreForm()
	return render_template(
	"edit-titre.html", form=f, lien='create', lien2='titre'
	)

@app.route('/titre/delete/<int:id>')
def supprimer_titre(id):
	nom = Requetes.get_titre_id(id)
	db.session.delete(nom)
	db.session.commit()
	return redirect(url_for('page_titre'))

@app.route("/save/titre/",methods=("POST",))
def save_Titre():
	a = None
	f = TitreForm()
	if f.validate_on_submit():
		# On recupere valeur de l'id dans le formulaire
		id = int(f.id.data)
		a = Requetes.get_titre_id(id)
		a.title = f.title.data
		db.session.commit()
		return redirect(url_for('one_titre', id=a.id))
	a = Requetes.get_titre_id(int(f.id.data))
	return render_template("accueil.html", artiste=a, form=f)

# ajout d'un artiste
@app.route('/create/titre/', methods=("POST",))
def ajouter_titre():
	a = None
	f = TitreForm()
	if f.validate_on_submit():
		name = str(f.title.data)
		# On recupere le nom (dans une requete avec id) permettant de comparer avec le resultat precedant obtenu
		req = Requetes.get_titre_title(name)
		if len(req) == 0:
			# On ajoute le nom dans la table Artiste
			ajout= Titre(title = name)
			db.session.add(ajout)
			db.session.commit()
			print('enregistre')
		else:
			print('pas enregistre')
	# On se redirige vers la page artiste
	return redirect(url_for('page_titre'))


### Vue Musique
@app.route('/consultation/')
def page_musique():
	return render_template('accueil-music.html', title="musique", accueil=Requetes.get_musiques_all(), affiche=False)
@app.route('/musique/annee/<int:year>')
def page_musique_year(year):
	return render_template('accueil-music.html', title="musique", accueil=Requetes.get_musiques_all(), year=year, affiche=True)

@app.route('/consultation/<int:id>')
def one_musique(id):
	return render_template('musique3.html', musics=Requetes.get_musique_id(id), lien='musique', id=id)

@app.route('/musique/edit/<int:id>')
def edit_musique(id):
	a = Requetes.get_musique_id(id)
	f = MusiqueForm(entryId=a.entryId, titre_id=a.titre_id, artiste_id=a.artiste_id, img=a.img, releaseYear=a.releaseYear, parent=a.parent)
	return render_template(
	"edit-musique.html", form=f, artiste=a, lien='save', lien2='musique'
	)

@app.route("/save/musique/",methods=("POST",))
def save_Musique():
	a = None
	f = MusiqueForm()
	if f.validate_on_submit():
		# On recupere valeur de l'id dans le formulaire
		id = int(f.entryId.data)
		a = Requetes.get_musique_id(id)
		a.img=f.img.data
		a.releaseYear=f.releaseYear.data
		a.parent=f.parent.data
		a.titre_id=int(f.titre_id.data)
		a.artiste_id=int(f.artiste_id.data)
		db.session.commit()
	return redirect(url_for('page_musique'))

@app.route('/consultation/create/')
def create_Musique():
	f= MusiqueForm()
	return render_template(
	"edit-musique.html", form=f, lien='create', lien2='musique'
	)

# ajout d'un artiste
@app.route('/consultation/c/musique/', methods=("POST",))
def ajouter_Musique():
	a = None
	f = MusiqueForm()
	if f.validate_on_submit():
		entryId=str(f.entryId.data)
		img=str(f.img.data)
		releaseYear=str(f.releaseYear.data)
		parent=str(f.parent.data)
		ajout = Musique(entryId=entryId, img=img, releaseYear=releaseYear, parent=parent)
		db.session.add(ajout)
		db.session.commit()
	# On se redirige vers la page artiste
	return redirect(url_for('homepage'))

@app.route('/consultation/delete/<int:id>')
def supprimer_Musique(id):
	nom = Requetes.get_artiste_id(id)
    #nom=Requetes.get_musiques_all
	db.session.delete(nom)
	db.session.commit()
	return redirect(url_for('page_musique'))

##### Vue Genres
@app.route('/genre/')
def page_genre():
	return render_template('accueil.html', title="genre", accueil=Requetes.get_genres_all())

@app.route('/genre/<int:id>')
def one_genre_id(id):
	return render_template('musique2.html', title=Requetes.get_genre_id(id), musics=Requetes.get_musiques_genre_id(id), id=id, line='genre')


@app.route('/genre/edit/<int:id>')
def edit_Genre(id):
	a = Requetes.get_genre_id(id)
	f = GenreForm(id=a.id, genre=a.genre)
	return render_template(
	"edit-genre.html",
	artiste=a, form=f, lien='save', lien2='genre'
	)

@app.route('/genre/create/')
def create_Genre():
	f= GenreForm()
	return render_template(
	"edit-genre.html", form=f, lien='create', lien2='genre'
	)

@app.route('/genre/delete/<int:id>')
def supprimer_genre(id):
	nom = Requetes.get_genre_id(id)
	db.session.delete(nom)
	db.session.commit()
	return redirect(url_for('page_genre'))

@app.route("/save/genre/",methods=("POST",))
def save_Genre():
	a = None
	f = GenreForm()
	if f.validate_on_submit():
		# On recupere valeur de l'id dans le formulaire
		id = int(f.id.data)
		a = Requetes.get_genre_id(id)
		a.genre = f.genre.data
		db.session.commit()
		return redirect(url_for('one_genre_id', id=a.id))
	return render_template('page_genre')

# ajout d'un artiste
@app.route('/create/genre/', methods=("POST",))
def ajouter_genre():
	a = None
	f = GenreForm()
	if f.validate_on_submit():
		ajout= Genre(genre = str(f.genre.data))
		db.session.add(ajout)
		db.session.commit()
	return redirect(url_for('page_genre'))
