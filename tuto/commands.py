from .app import db, manager
from .models import Artiste, Titre, Genre, Musique, Combinaison

import yaml

@manager.command
def syncdb():
	'''Creates the tables'''
	db.create_all()


@manager.command
def loaddb(filename):
	''' Creates the tables and populates them with data '''
	db.create_all()
	musicData = yaml.load(open(filename))

	groupArtiste = {}
	for m in musicData :
		a = m["by"]
		if a not in groupArtiste:
			newArtiste = Artiste(by = m["by"])
			db.session.add(newArtiste)
			groupArtiste[a] = newArtiste
	db.session.commit()

	groupTitre = {}
	for m in musicData :
		t = m["title"]
		if t not in groupTitre :
			newTitre = Titre(title = m["title"])
			db.session.add(newTitre)
			groupTitre[t] = newTitre
	db.session.commit()


	#Dictonnaire de genres
	groupGenre = {}
	#Pour tout element de la bdd
	for m in musicData :
		#On recupere la liste de genre dans une variable 'genre'
		genre= m["genre"]
		#Pour tout element de cette liste de genre ex : [genre1, genre2]
		for g in range(len(genre)):
			#Si un element ex: genre1 de la liste [genre1, genre2] n'est pas present dans le dico
			if genre[g] not in groupGenre :
				#On instancie un nouveau genre
				newGenre = Genre(genre = genre[g])
				#On ajoute le genre dans le dico
				groupGenre[genre[g]] = newGenre
				#On ajoute le genre dans le dico
				db.session.add(newGenre)
	db.session.commit()

	#Creation de toutes les musiques
	groupMusique={}
	for m in musicData :
		a = groupArtiste[m["by"]]
		t = groupTitre[m["title"]]
		mus = Musique(entryId = m["entryId"],
			img = m["img"],
			parent = m["parent"],
			releaseYear= m["releaseYear"],
			titre_id = t.id,
			artiste_id = a.id)
		groupMusique[m["entryId"]] = mus
		db.session.add(mus)
	db.session.commit()

	#Combinaison musique et genre
	for m in musicData :
		mus = groupMusique[m["entryId"]]
		genre = m["genre"]
		for g in range(len(genre)):
			res = groupGenre[genre[g]]
			comb = Combinaison(musique_id = mus.entryId, genre_id = res.id)
			db.session.add(comb)
	db.session.commit()

@manager.command
def affichebd(nomTable):
	'''Affichage du contenu de la table indique '''
	print("######################")
	if nomTable == ("Artiste" or "artiste" or "Artist" or "artist"):
		affichageA = Artiste.query.all()
		print("Artiste")
		print(affichageA)
	elif nomTable == ("Titre" or "titre" or "Title" or "title"):
		affichageT = Titre.query.all()
		print("Titre")
		print(affichageT)
	elif nomTable == ("Musique" or "musique" or "Music" or "music"):
		affichageM = Musique.query.all()
		print("Musique")
		print(affichageM)
	elif nomTable == ("Genre" or "genre"):
		affichageG = Genre.query.all()
		print("Genre")
		print(affichageG)
	else :
		affichageC = Combinaison.query.all()
		print("Combinaison")
		print(affichageC)


# ajout de la commande newuser
@manager.command
def newuser(username, password):
    '''ajout d'un autre utilisateur'''
    from .models import User
    from hashlib import sha256
    m=sha256()
    m.update(password.encode())
    u = User(username=username, password=m.hexdigest())
    db.session.add(u)
    db.session.commit()
