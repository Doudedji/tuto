from .app import db


# Tables et Colonnes des tables
class Artiste (db.Model):
	id = db.Column(db.Integer, primary_key = True)
	by = db.Column(db.String(300))

	def __repr__(self):
		return "%s" % (self.by)


class Titre (db.Model):
	id = db.Column(db.Integer, primary_key = True)
	title = db.Column(db.String(500))

	def __repr__(self):
		return "%s" % (self.title)


class Genre (db.Model):
	id = db.Column(db.Integer, primary_key = True)
	genre = db.Column(db.String(500))

	def __repr__(self):
		return "%s" % (self.genre)


class Musique (db.Model):
	entryId = db.Column(db.Integer, primary_key=True)
	img = db.Column(db.String(500))
	parent = db.Column(db.String(300))
	releaseYear = db.Column(db.Integer)
	artiste_id = db.Column(db.Integer, db.ForeignKey("artiste.id"))
	titre_id = db.Column(db.Integer, db.ForeignKey("titre.id"))
	artiste = db.relationship("Artiste", backref=db.backref("musicData", lazy="dynamic"))
	titre = db.relationship("Titre", backref=db.backref("musicData", lazy="dynamic"))

	def __repr__(self):
		return "<Musique (%d)  :  %s | %s | %s | %s | %s >" % (self.entryId, self.img, self.parent, self.releaseYear, self.artiste, self.titre)

class Combinaison (db.Model):
	id = db.Column(db.Integer, primary_key=True)
	musique_id = db.Column(db.Integer, db.ForeignKey("musique.entryId"))
	genre_id = db.Column(db.Integer, db.ForeignKey("genre.id"))
	musique = db.relationship("Musique", backref=db.backref("musique_asso", lazy="dynamic"))
	genre = db.relationship("Genre", backref=db.backref("genre_asso", lazy="dynamic"))

	def __repr__(self):
		return "%s | %s" % (self.musique, self.genre)


class Requetes():

	#Getters Artistes
	def get_artiste_id(id):
		return Artiste.query.get_or_404(id)

	def get_artiste_name(by):
		return Artiste.query.filter(Artiste.by==by).all()

	def get_artistes_all():
		return Artiste.query.all()


	#Getters Titre
	def get_titre_id(id):
		return Titre.query.get_or_404(id)

	def get_titre_title(title):
		return Titre.query.filter(Titre.title==title).all()

	def get_titres_all():
		return Titre.query.all()


	#Getters Genres
	def get_genre_id(id):
		return Genre.query.get_or_404(id)

	def get_genre_name(genre):
		return Genre.query.filter(Genre.genre==genre).all()

	def get_genres_all():
		return Genre.query.all()


	#Getters Musiques et Combinaison
	def get_musiques_artiste_id(id):
		return Musique.query.filter(Musique.artiste==Artiste.query.get_or_404(id)).all()

	def get_musiques_titre_id(id):
		return Musique.query.filter(Musique.titre==Titre.query.get_or_404(id)).all()

	def get_musiques_titre_id_one(id):
		return Musique.query.filter(Musique.titre==Titre.query.get_or_404(id)).one()

	def get_musiques_artiste_name(name):
		return Musique.query.filter(Musique.artiste==Artiste.query.filter(name).all()).all()


	def get_musiques_genre_id(id):
		return Combinaison.query.filter(Combinaison.genre==Genre.query.get_or_404(id)).all()

	def get_musiques_genre_name(name):
		return Combinaison.query.filter(Combinaison.genre==Genre.query.filter(Genre.genre==genre)).all()


	def get_musique_id(id):
		return Musique.query.get_or_404(id)

	def get_musique_complete_id(id):
		return Combinaison.query.filter(Combinaison.id==id).all()

	def get_musique_complete_id_one(id):
		return Combinaison.query.filter(Combinaison.id==id).one()

	def get_musiques_all():
		return Musique.query.all()

	def get_combinaison_all():
		return Combinaison.query.all()

	def get_annee_musique(year):
		return Musique.query.filter(Musique.releaseYear==year).all()

	def get_annee_musique_plus(year):
		return Musique.query.filter(Musique.releaseYear>year).all()

	def get_annee_musique_moins(year):
		return Musique.query.filter(Musique.releaseYear<year).all()


	#Echantillon
	def get_sample():
		return Musique.query.limit(5).all()


def get_id(self):
	return self.username

#Ajout u callback
from .app import login_manager

@login_manager.user_loader
def load_user(username):
	return User.query.get(username)
